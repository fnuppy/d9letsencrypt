#!/usr/bin/env bash

NFSPATH=$2
AEGIR="aegir"
PLATFORM=$3
SITE=$4
SITEPATH=$5

if [ -d "$NFSPATH" ]; then
  echo "$NFSPATH exist"
else
  echo "$NFSPATH nfs path not exist or not correct! Fail"
  exit 1
fi

parentdir=$NFSPATH
for dirrectory in $AEGIR $PLATFORM $SITE
  do

  parentdir="${parentdir}/${dirrectory}"

  if [ -d "$parentdir" ]; then
    echo "$parentdir exist"
  else
    mkdir $parentdir
  fi
done

case "$1" in
  -install) OPERATION='install'
  ;;
  -clone) OPERATION='clone'
  ;;
  -migrate) OPERATION='migrate'
  ;;
  *) echo "not correct operation name"
  exit 1
  ;;
esac

if [ -d "$SITEPATH" ]; then
  echo "Site path $SITEPATH exist"
else
  echo "Site path $SITEPATH not exist. Exit!"
  exit 1
fi

for storagetype in "files" "private"
  do
	if [ -d "$SITEPATH/$storagetype" ] && [ -L "$SITEPATH/$storagetype" ] && [ -e "$SITEPATH/$storagetype" ]
	then
		echo "$SITEPATH/$storagetype link already exist. Skip!"
	elif [ -d "$SITEPATH/$storagetype" ] && [ -d "$parentdir/$storagetype" ]
	then
		echo "$parentdir/$storagetype folder already exist in nfs. I will try to generate symlink"
                rsync -az --exclude 'temp' $SITEPATH/$storagetype/ $parentdir/$storagetype
		rm -rf $SITEPATH/$storagetype
		ln -s $parentdir/$storagetype $SITEPATH/$storagetype
	elif [ -d "$SITEPATH/$storagetype" ] && [ ! -d "$parentdir/$storagetype" ]
	then
		echo "$parentdir/$storagetype folder not exist. Will move folder and will create symlink"
		mv $SITEPATH/$storagetype $parentdir/$storagetype
		ln -s $parentdir/$storagetype $SITEPATH/$storagetype
	elif [ ! -d "$SITEPATH/$storagetype" ]
	then
		echo "$parentdir/$storagetype folder not exist. Error"
		exit 1
	fi
done
